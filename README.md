# Product Statistics

This extension provides data about how many times a product has been viewed or
ordered.

## How to use

Data can be retrieved with ajax by calling the following url:

    /productstats/product/flash/id/:productId

### Vue.js

Optionally there is a JS file included that requires Vue.JS. Include this JS file
and then use the following HTML anywhere on the site to display the message:

    <div id="product-stats">
        <product-stats :product-id="1234"></product-stats>
    </div>
