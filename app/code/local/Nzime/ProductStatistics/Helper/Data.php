<?php
class Nzime_ProductStatistics_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $resource;
	private $readConnection;
	    
    public function connect()
    {
        if ($this->resource) return;
        
        $this->resource = Mage::getSingleton('core/resource');
	    $this->readConnection = $this->resource->getConnection('core_read');
    }
    
    public function getViewsToday($productId)
    {
        $this->connect();
        
        $query = sprintf('SELECT COUNT(*) FROM %s WHERE product_id = %d AND added_at >= \'%s\'', 
            $this->resource->getTableName('report_viewed_product_index'),
            (int)$productId,
            $this->today()
        );
        
	    return (int)$this->readConnection->fetchOne($query);
    }
    
    public function getViewsWeek($productId)
    {
        $this->connect();
        
        $query = sprintf('SELECT COUNT(*) FROM %s WHERE product_id = %d AND added_at >= \'%s\'', 
            $this->resource->getTableName('report_viewed_product_index'),
            (int)$productId,
            $this->today('-1 week')
        );
        
	    return (int)$this->readConnection->fetchOne($query);
    }
    
    public function getOrdersWeek($productIds = array())
    {
        $this->connect();
        
        $query = sprintf('select COUNT(*) from %s WHERE product_id IN (%s) AND created_at >= \'%s\' GROUP BY order_id', 
            $this->resource->getTableName('sales_flat_order_item'), 
            implode(',', $productIds),
            $this->today('-1 week')
        );
        
	    return (int)$this->readConnection->fetchOne($query);
    }
    
    private function today($modify = null, $format = 'Y-m-d')
    {
        $date = new DateTime();
        if ($modify)
            $date->modify($modify);
        
        return $date->format($format);
    }
}
	 