<?php

class Nzime_ProductStatistics_ProductController extends Mage_Core_Controller_Front_Action 
{        
    public function flashAction() 
    {
        $helper = Mage::helper('productstatistics');
        
        $productId = $this->getRequest()->getParam('id');
        $childIds = [$productId];
        
        // Get parent and then all children
        $parentId = array_pop(Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId));
        if (!is_null($parentId)) {
            $productId = $parentId;
            $childIds = array_pop(Mage::getModel('catalog/product_type_configurable')->getChildrenIds($parentId));
        }
        
        echo json_encode([
            'views' => [
                'today' => $helper->getViewsToday($productId),
                'week' => $helper->getViewsWeek($productId)
            ],
            'orders' => [
                'week' => $helper->getOrdersWeek($childIds)
            ]
        ]);
    }
}