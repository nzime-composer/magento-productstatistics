window.addEventListener('load', function () {
    if (document.getElementById('product-stats')) {
        
        var Stats = {
            template: '<div v-show="show" v-on:click="hide">{{ response }}</div>',
            data: function() {
                return {
                    response: null,
                    show: true
                }
            },
            props: {
                productId: Number
            },
            created: function() {
                var self = this;
                
                this.$http.get('/productstats/product/flash/id/'+this.productId).then(function(response) {
                    var data = response.data;
                    
                    if (data.views.today > 1)
                        this.response = data.views.today + ' people have viewed this product today';
                    else if (data.views.week > 1)
                        this.response = data.views.week + ' people have viewed this product in the last week';
                        
                    // Show only for X seconds
                    setTimeout(function() { 
                        self.show = false;
                    }, 5000)
                });
            },
            methods: {
                hide: function() {
                    this.show = false;
                }
            }
        }
        
        new Vue({
            el: '#product-stats',
            components: {
                'product-stats': Stats
            }
        });
    }
});